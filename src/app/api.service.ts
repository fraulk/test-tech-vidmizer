import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = "https://geo.api.gouv.fr/regions"

  constructor(private http: HttpClient) { }

  getRegions(): Observable<any> {
    return this.http.get(this.baseUrl)
  }
}
