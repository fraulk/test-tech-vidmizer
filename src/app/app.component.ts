import { Component } from '@angular/core';
import { ApiService } from './api.service'
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'test-tech-vidmizer';
  regions = []
  form
  data = []
  search: string

  constructor(private apiService: ApiService, private formBuilder: FormBuilder,) {
    this.form = formBuilder.group({
      lastname: '',
      firstname: '',
      phone: '',
      region: ''
    })
  }

  getRegions() {
    this.apiService.getRegions().subscribe(
      (regions) => this.regions = regions,
      (err) => console.log(err),
      () => console.log("getRegion done")
    )
  }

  ngOnInit() {
    this.getRegions()
    this.data = (localStorage.getItem("data") != null) ? JSON.parse(localStorage.getItem("data")) : []
  }

  onSubmit(inputData) {
    let duplicate = false
    this.data.map(item => {
      if (item.lastname == inputData.lastname || item.phone == inputData.phone)
        duplicate = true
    })
    if (duplicate) return
    this.data.push(inputData)
    this.data.sort((a, b) => (a.lastname > b.lastname) ? 1 : ((b.lastname > a.lastname) ? -1 : 0))
    console.log(inputData)
    this.form.reset();
    localStorage.setItem("data", JSON.stringify(this.data))
  }

  deleteElement(index) {
    this.data.splice(index, 1)
    localStorage.setItem("data", JSON.stringify(this.data))
  }
}
