import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (!args) {
            return value;
        }
        return value.filter((val) => {
            let rVal = (val.lastname.toLocaleLowerCase().includes(args)) || (val.firstname.toLocaleLowerCase().includes(args)) || (val.phone.toLocaleLowerCase().includes(args)) || (val.region.toLocaleLowerCase().includes(args));
            return rVal;
        })

    }

}